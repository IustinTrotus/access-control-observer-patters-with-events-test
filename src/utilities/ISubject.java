/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 * An interface to make a class a "subject which can be observed"
 */
public interface ISubject extends IEvent{
    
    Boolean registerObserver(IObserver observer);
    Boolean removeObserver(IObserver observer);
    void notifyObservers();
    
    /**
     *Takes an iSubject and notifies its own observers about changes in it
     * 
     * @param iSubject - the iSubject to pass to its own observers
     */
    void notifyObservers(ISubject iSubject);
    
}
