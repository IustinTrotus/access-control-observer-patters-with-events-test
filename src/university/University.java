/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package university;

import campus.campus.Campus;
import java.util.ArrayList;
import java.util.List;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import utilities.IObserver;
import utilities.ISubject;
import utilities.ISubjectEvent;

/**
 *
 * @author Iustin
 */
public class University implements IOperatingMode, IObserver, ISubject {

    private List<Campus> campuses = new ArrayList<>();
    private ModeManager modeManager = new ModeManager();
    private ISubjectEvent event = ISubjectEvent.None;
    private List<IObserver> observers = new ArrayList<>();

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        throw new UnsupportedOperationException("Not supported yet.");
        //
    }

    @Override
    public Boolean registerObserver (IObserver observer) {
        return this.observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        return this.observers.remove(observer);
    }

    @Override
    public void update (ISubject iSubject) {
        notifyObservers(iSubject);
    }

    @Override
    public void notifyObservers () {
        for ( IObserver observer : observers ) {
            observer.update(this);
        }
    }

    @Override
    public void notifyObservers (ISubject iSubject) {
        for ( IObserver observer : observers ) {
            observer.update(iSubject);
        }
    }

    @Override
    public ISubjectEvent getEvent () {
        return this.event;
    }

    @Override
    public void setEvent (ISubjectEvent newEvent) {
        this.event = newEvent;
    }

}
