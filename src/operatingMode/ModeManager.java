/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operatingMode;

/**
 *
 * @author Iustin
 */
public class ModeManager implements IOperatingMode {

    private OperatingMode operatingMode;

    //the forceEmergency boolean can be set to true  only when the 
    private boolean forceEmengencyMode;

    public ModeManager() {
        this.operatingMode = OperatingMode.Normal;
    }

    @Override
    public OperatingMode getOperatingMode() {
        return this.operatingMode;
    }

    /**
     *
     * @param operatingMode
     */
    @Override
    public void setOperatingMode(OperatingMode operatingMode) {
        if (operatingMode == null) {
            throw new NullPointerException();
        }

        if (this.operatingMode != operatingMode) {

            if (operatingMode == OperatingMode.Emergency) {
                this.operatingMode = OperatingMode.Emergency;

            //if the new operating mode is not Emergency
            } else if (forceEmengencyMode == false) {
                this.operatingMode = operatingMode;
            }

        }

    }

    @Override
    public void setOperatingMode(OperatingMode operatingMode, boolean forceEmergencyMode) {
        if (operatingMode == null) {
            throw new NullPointerException();
        }

        if (forceEmergencyMode && operatingMode == OperatingMode.Normal) {
            throw new IllegalArgumentException("Cannot force Emergency mode and set the operating mode to Normal");
        }

        setOperatingMode(operatingMode);

        setForceEmergency(forceEmergencyMode);

    }

    private void setForceEmergency(boolean flag) {
        OperatingMode currentMode = getOperatingMode();

        if (flag == true) {
            if (currentMode == OperatingMode.Emergency) {
                forceEmengencyMode = true;
            }
            //if flag is false
        } else {
            forceEmengencyMode = false;
        }
    }

}
