/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.building;

import campus.room.Room;
import java.util.ArrayList;
import java.util.List;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import utilities.IObserver;
import utilities.ISubject;
import utilities.ISubjectEvent;

/**
 *
 * @author Iustin
 */
public class Building implements ISubject, IObserver, IOperatingMode {

    private List<IObserver> observers = new ArrayList<>();
    private ISubjectEvent event = ISubjectEvent.None;
    private ModeManager modeManager = new ModeManager();

    private List<Room> rooms = new ArrayList<>();

    public void addRoom (Room room) {
        rooms.add(room);
        registerObserver(room);
        room.registerObserver(this);
    }

    @Override
    public Boolean registerObserver (IObserver observer) {

        System.out.println("Building ADD Observer");
        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        System.out.println("Building REMOVE Observer");
        return observers.remove(observer);
    }

    @Override
    public void notifyObservers () {

        System.out.println("Building notify Observer START");
        for ( IObserver observer : observers ) {
            observer.update(this);
        }

        System.out.println("Building notify Observer END");
    }

    @Override
    public void notifyObservers (ISubject iSubject) {
        System.out.println("Building notify Observer START Pass " + iSubject.toString());
        
        for ( IObserver observer : observers ) {
            observer.update(iSubject);
        }
        System.out.println("Building notify Observer END Pass" + iSubject.toString());
    }

    @Override
    public void update (ISubject iSubject) {
        ISubjectEvent iSubjectEvent = iSubject.getEvent();

        if ( null != iSubjectEvent ) {
            switch ( iSubjectEvent ) {
                case CampusModeChange:
                    setOperatingMode((( IOperatingMode ) (iSubject)).getOperatingMode());
                    break;
                case RoomModeChange:
                    iSubject.setEvent(ISubjectEvent.SignalRoomModeChange);
                    notifyObservers(iSubject);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public ISubjectEvent getEvent () {
        System.out.println("Building get event " + event);
        return this.event;

    }

    @Override
    public void setEvent (ISubjectEvent newEvent) {
        System.out.println("Building set event " + newEvent);

        this.event = newEvent;
        if ( newEvent != ISubjectEvent.SignalBuidingModeChange ) {

            notifyObservers();
            this.event = ISubjectEvent.None;

            System.out.println("Building set event NONE");
        }
    }

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();

    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
        setEvent(ISubjectEvent.BuidingModeChange);
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        throw new UnsupportedOperationException("Not supported yet.");
        //
    }

}
