/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.campus;

import campus.building.Building;
import java.util.ArrayList;
import java.util.List;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import utilities.IObserver;
import utilities.ISubject;
import utilities.ISubjectEvent;

/**
 *
 * @author Iustin
 */
public class Campus implements ISubject, IObserver, IOperatingMode {

    private List<IObserver> observers = new ArrayList<>();
    private ISubjectEvent event = ISubjectEvent.None;
    private ModeManager modeManager = new ModeManager();

    private List<Building> buildings = new ArrayList<>();

    public void addBuilding (Building building) {
        buildings.add(building);
        registerObserver(building);
        building.registerObserver(this);
    }

    @Override
    public Boolean registerObserver (IObserver observer) {

        System.out.println("Campus ADD Observer");
        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        System.out.println("Campus REMOVE Observer");
        return observers.remove(observer);
    }

    @Override
    public void notifyObservers () {

        System.out.println("Campus notify Observer START");
        for ( IObserver observer : observers ) {
            observer.update(this);
        }

        System.out.println("Campus notify Observer END");
    }

    @Override
    public void notifyObservers (ISubject iSubject) {
        System.out.println("Campus notify Observer START Pass " + iSubject.toString());
        for ( IObserver observer : observers ) {
            observer.update(iSubject);
        }

        System.out.println("Campus notify Observer 2 END Pass" + iSubject.toString());

    }

    @Override
    public void update (ISubject iSubject) {
        ISubjectEvent iSubjectEvent = iSubject.getEvent();

        if ( null != iSubjectEvent ) {
            switch ( iSubjectEvent ) {
                case UniversityModeChange:
                    setOperatingMode((( IOperatingMode ) (iSubject)).getOperatingMode());
                    break;
                case SignalRoomModeChange:
                    notifyObservers(iSubject);
                    break;
                case BuidingModeChange:
                    iSubject.setEvent(ISubjectEvent.SignalBuidingModeChange);
                    notifyObservers(iSubject);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public ISubjectEvent getEvent () {
        System.out.println("Campus get event " + event);
        return this.event;

    }

    @Override
    public void setEvent (ISubjectEvent newEvent) {
        this.event = newEvent;
        System.out.println("Campus set event " + newEvent);

        notifyObservers();
        this.event = ISubjectEvent.None;

        System.out.println("Campus set event NONE");
    }

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();

    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
        setEvent(ISubjectEvent.CampusModeChange);
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        throw new UnsupportedOperationException("Not supported yet.");
        //
    }

}
