/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.room;

import java.util.ArrayList;
import java.util.List;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import utilities.IObserver;
import utilities.ISubject;
import utilities.ISubjectEvent;

/**
 *
 * @author Iustin
 */
public class Room implements ISubject, IObserver, IOperatingMode {

    private List<IObserver> observers = new ArrayList<>();
    private ISubjectEvent event = ISubjectEvent.None;
    private ModeManager modeManager = new ModeManager();

    @Override
    public Boolean registerObserver (IObserver observer) {

        System.out.println("Room ADD Observer");
        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        System.out.println("Room REMOVE Observer");
        return observers.remove(observer);
    }

    @Override
    public void notifyObservers () {

        System.out.println("Room notify Observer START");
        for ( IObserver observer : observers ) {
            observer.update(this);
        }
        System.out.println("Room notify Observer END");
    }

    @Override
    public ISubjectEvent getEvent () {
        System.out.println("Room get event " + event);
        return this.event;

    }

    @Override
    public void setEvent (ISubjectEvent newEvent) {
        this.event = newEvent;
        System.out.println("Room set event " + newEvent);
        if ( newEvent != ISubjectEvent.SignalRoomModeChange ) {
            notifyObservers();

        this.event = ISubjectEvent.None;
        System.out.println("Room set event NONE");
        }
    }

    @Override
    public void update (ISubject iSubject) {
        if ( iSubject.getEvent() == ISubjectEvent.BuidingModeChange ) {
            setOperatingMode((( IOperatingMode ) (iSubject)).getOperatingMode());
        }
    }

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();

    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
        setEvent(ISubjectEvent.RoomModeChange);
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        throw new UnsupportedOperationException("Not supported yet.");
        //
    }

    @Override
    public void notifyObservers (ISubject iSubject) {
        throw new UnsupportedOperationException("Not supported yet.");
        //
    }

}
